export const LoadingComponent = () => {

	// mock a fake scenario of loading...
	const timeout = setTimeout(() => m.route.set('/results'), 2000);

	return {
		onremove: () => clearTimeout(timeout),

		view: () => m('.loading',
			m('p', 'Loading...')
		)
	}
}
